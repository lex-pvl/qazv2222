
$(document).ready(function() {
    $(".m-bg-cont").each(function() {
		var img = $(this).find("img").attr("src");
		$(this).css("background-image", "url(" + img + ")");
    });

    $(".tabs").each(function() {
		$('.tabs__tab').click(function(e) {
			e.preventDefault();
			var it = $(this);
			var href = it.attr("href");
			it.closest(".tabs__tabs").find(".tabs__tab").removeClass("active");
			it.addClass("active");
			$(".tabs__block").removeClass("active");
			it.closest(".tabs").find("." + href).addClass('active');
		});
	});

	$('blockquote, cite, .cite').each(function() {
		$(this).append('<svg class="icon__cite-svg" width="32px" height="24px"><use xlink:href="#cite-svg"></use></svg>');
	});


	var swiper = new Swiper('.newsslider__items', {
		spaceBetween: 30,
		speed: 300,
		parallax: true,
		slidesPerView: 2,
		navigation: {
			nextEl: '.newsslider__slider-button-prev',
			prevEl: '.newsslider__slider-button-next',
		},
		pagination: {
			el: '.first-pagination',
		},
		breakpoints: {
			1025: {
				slidesPerView: 2,
			},
			1024: {
				slidesPerView: 1,
			},
			768: {
				slidesPerView: 1,	
			},
		}
	});
	
	var hash = window.location.hash;
	console.log(hash);
	if(hash.length > 0) {
		$(".cont-tab").removeClass("active");
		$(".tab").removeClass("active");;
		$(hash).addClass('active');
		$(hash).val('');
		$(hash).removeClass("is-focus");
		var index = $(".cont-tab.active").index() + 1;
		console.log(index);
		$(".tab:nth-child("+index+")").addClass("active");
	}
	$(".tabs").each(function() {
		$('.tab').click(function(e) {
			e.preventDefault();
			var it = $(this);
			var href = it.attr("href");
			it.closest(".tabs-container").find(".tab").removeClass("active");
			it.addClass("active");
			$(".cont-tab").removeClass("active");
			$(href).each(function () {
				it.closest(".tabs").find(href).addClass('active');
				it.closest('.tabs').find(href + " input").val('');
				it.closest('.tabs').find(href + " input").removeClass("is-focus");
			});
		});
	});


 //    var myMap;
	// var myPlacemark = [];

    
 //    if ($("#map").length > 0)  {

	// 	ymaps.ready(init);

	// 	function init () {
	// 		var map = $('#map');
	// 		var lat = map.data('start-lat');
	// 		var lng = map.data('start-lng');
	// 		var zoom = map.data('zoom');
	// 		var pointer = map.data('pointer');

	// 		myMap = new ymaps.Map('map', {
	// 			center: [lat, lng],
	// 			zoom: zoom,
	// 			controls: []
	// 		}, {
	// 			searchControlProvider: 'yandex#search'
	// 		});

	// 		for(var i = 0; i < mapsMarkers.length; i++) {
	// 			var title = mapsMarkers[i].title;
	// 			var text = mapsMarkers[i].text;
	// 			var lng = mapsMarkers[i].lng;
	// 			var lat = mapsMarkers[i].lat;
	// 			var id = mapsMarkers[i].id;

	// 			myPlacemark[id] = new ymaps.Placemark([lat, lng], {
	// 				hintContent: title,
	// 			}, {
	// 				iconLayout: 'default#image',
	// 				// iconImageHref: pointer,
	// 				// iconImageSize: [40, 40],
	// 				// iconImageOffset: [-20, -30]
	// 			});
	// 			myMap.geoObjects.add(myPlacemark[id]);
	// 		};

	// 	};
 //    };
    
    
    
});
