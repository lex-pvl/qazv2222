$('ul.s-about__tabs').on('click', 'li:not(.active)', function() {
	$(this)
	.addClass('active').siblings().removeClass('active')
	.closest('div.s-public-about-tabs').find('div.s-about__content').removeClass('active').eq($(this).index()).addClass('active');
});

function reviewForm() {
	$('#review-focus').on('focus', function() {
		$(this).parents('form').fadeOut(400);
		$('.l-review-block').slideDown(400);
		$('#review-text').focus();
	});

	$('#review-cancel').on('click', function(event) {
		event.preventDefault();
		$('.l-review-block').slideUp(400);
		$('#review-focus').parents('form').fadeIn('slow');
	});
}
reviewForm();

var actionGallery = new Swiper('.l-action-gallery-container', {
	slidesPerView: 3,
	spaceBetween: 58,
	cssMode: true,
	navigation: {
		nextEl: '.l-action-gallery-next',
		prevEl: '.l-action-gallery-prev',
	},
});

// $('#l-file').on('input', function() {
// 	console.log( $(this) )
// });

function downloadFiles(input, jqueryThis) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			jqueryThis.closest('.l-download-img').find('.l-download-img__item img').attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}
}
$("#l-file").on('change', function(){
	downloadFiles(this, $(this));
});
function crop() {
	const limit = 150;
	const textSelector = '.l-review-comment span';
	$(textSelector).each(function(){
		const textBlock = $(this);
		if($(this).text().length > limit){
			const allText = $(this).html();
			$(this).html($(this).html().substr(0, limit) + '... <a href="#" class="l-show-full">показать полностью</a>');
			$(this).find('a.l-show-full').on('click', function(e){
				e.preventDefault();
				textBlock.html(allText);
			});
		}
	});
}
crop();

// (function(){
// 	$.mask.definitions['H'] = "[0-2]";
// 	$.mask.definitions['h'] = "[0-9]";
// 	$.mask.definitions['M'] = "[0-5]";
// 	$.mask.definitions['m'] = "[0-9]";

// 	$("#calendar--input").mask("Hh:Mm", {
// 		completed: function() {
// 			var currentMask = $(this).mask();
// 			if (isNaN(parseInt(currentMask))) {
// 				$(this).val("");
// 			} else if (parseInt(currentMask) > 2359) {
// 				$(this).val("23:59");
// 			};
// 		}
// 	});
// })

$('ul.contacts__list').on('click', 'li:not(.active)', function() {
	$(this)
	.addClass('active').siblings().removeClass('active')
	.closest('div.contacts__tabs').find('div.contacts__item').removeClass('active').eq($(this).index()).addClass('active');
});

function showVariants() {
	var center = $('#contacts-centers');
	var office = $('#contacts-office');

	center.on('click', function(){
		$('.city-check').addClass('city-check-active');
	});
	office.on('click', function() {
		$('.city-check').removeClass('city-check-active');
	});
};
showVariants();

function showCities() {
	var check = $('#contacts-check');
	check.on('change', function() {
		if (!$('#contacts-check:checked').length) {
			$('.smap').hide();
			$('.contacts__item-2 .wrapper').show();
		} else {
			$('.smap').show();
			$('.contacts__item-2 .wrapper').hide();
		}
	});
}
showCities();

$(function(){
	if($('#map').is('#map')){
		var coor_lat = +$('#map').data('lat');
		var coor_lng = +$('#map').data('lng');
		var iconUrl = $('#map').data('icon-url');
		 ymaps.ready(init);

		function init() {
		    var map = new ymaps.Map('map', {
		        center: [coor_lat, coor_lng],
		        zoom: 18,
		        controls: [],
		        behaviors: ['drag']
		    });

		    var placemark = new ymaps.Placemark([coor_lat, coor_lng], {
		        hintContent: '<div class="hint-street">ул.Карасакал Еримбет 51</div>'
		    },
		    {
		        iconLayout: 'default#image',
		        iconImageHref: iconUrl,
		        iconImageSize: [51, 71]
		    });

		    map.geoObjects.add(placemark)
		}
	}
	if($('#smap').is('#smap')){
		var iconUrl = $('#smap').data('icon-url');
		ymaps.ready(sInit);
		function sInit() {
		    var smap = new ymaps.Map('smap', {
		        center: [48.53662506, 64.47742010],
		        zoom: 6,
		        controls: ['zoomControl'],
		        behaviors: ['drag']
		    });

		    if ( $(window).width() < 1500 ) {
		    	smap.setZoom(5)
		    }

		    if ( $(window).width() < 1000 ) {
		    	smap.setZoom(5)
		    }

		    city.forEach(function(elem) {
		        var cities = new ymaps.Placemark(elem.placemark, {
		            hintContent: `<div class="hint-street">${elem.hint}</div>`,
		            hideIconOnBalloonOpen: false,
		            balloonContent: `
		            <div class="balloon-city">
		                <h4>${elem.hint}</h4>
		                <p>${elem.place}</p>
		                <div>
		                    <a href="tel:${elem.tel}">${elem.phone}</a>
		                    <p>${elem.coord}</p>
		                </div>
		            </div>
		            `
		        },
		        {
		            iconLayout: 'default#image',
		            iconImageHref: iconUrl,
		            iconImageSize: [51, 51]
		        });
		        smap.geoObjects.add(cities);
		    })

		}
	}
})


$(function(){
    $(window).on('load scroll resize', function() {
        var o = $(window).scrollTop() / ( $(document).height() - $(window).height() );
        $('.progress-bar').css({
            width: (100 * o | 0) + "%"
        });
        $('progress').val(o);
    });
})

$('#history-select').select2({
	templateResult: formatState,
	placeholder: "Вы можете указать до 3-х направлений",
	maximumSelectionLength: 3,
	language: {
		maximumSelected: function (e) {
			var t = "Максимум " + e.maximum + " пункта";
			return t;
		}
	}
});