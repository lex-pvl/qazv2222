//glabal wars
var getArrayDays;
var getArrayMouth;
var mouthForSelect = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
var confettyRun = function(){
	var screenWidth = $(window).width();
	if (screenWidth > 799){
		var canvas = document.getElementById('my-canvas-left');
		var canvass = document.getElementById('my-canvas-right');
		canvas.confetti = canvas.confetti || confetti.create(canvas, {
		    resize: true
		});
		canvas.confetti({
		    spread: 50,
		    angle: 180,
		    origin: {
		        y: 0.25,
		        x: 1
		    }
		});
		canvass.confetti = canvass.confetti || confetti.create(canvass, {
		    resize: true
		});
		canvass.confetti({
		    spread: 50,
		    angle: 360,
		    origin: {
		        y: 0.25,
		        x: 0
		    }
		});
	}
}



var setBackgroundImage = function() {
	$('.set-bg-img').each(function(){
		if ($(this).find('.get-bg-img').is('.get-bg-img')) {
			var src = $(this).find('.get-bg-img').attr('src');
			$(this).css('background-image' , 'url(' + src + ')');
		};
	});
};
// var simplemaps_countrymap_mapdata = {
//   main_settings: {
//    //General settings
//     width: "1200", //'700' or 'responsive'
//     background_color: "#FFFFFF",
//     background_transparent: "yes",
//     border_color: "#ffffff",
    
//     //State defaults
//     state_description: "State description",
//     state_color: "#88A4BC",
//     state_hover_color: "#3B729F",
//     state_url: "",
//     border_size: 1.5,
//     all_states_inactive: "no",
//     all_states_zoomable: "no",
    
//     //Location defaults
//     location_description: "",
//     location_url: "",
//     location_color: "#FF0067",
//     location_opacity: 0.8,
//     location_hover_opacity: 1,
//     location_size: 25,
//     location_type: "square",
//     location_image_source: "frog.png",
//     location_border_color: "#FFFFFF",
//     location_border: 2,
//     location_hover_border: 2.5,
//     all_locations_inactive: "no",
//     all_locations_hidden: "no",
    
//     //Label defaults
//     label_color: "#d5ddec",
//     label_hover_color: "#d5ddec",
//     label_size: 22,
//     label_font: "Arial",
//     hide_labels: "no",
//     hide_eastern_labels: "no",
   
//     //Zoom settings
//     zoom: "yes",
//     manual_zoom: "no",
//     back_image: "no",
//     initial_back: "no",
//     initial_zoom: "-1",
//     initial_zoom_solo: "no",
//     region_opacity: 1,
//     region_hover_opacity: 0.6,
//     zoom_out_incrementally: "yes",
//     zoom_percentage: 0.99,
//     zoom_time: 0.5,
    
//     //Popup settings
//     popup_color: "white",
//     popup_opacity: 0.9,
//     popup_shadow: 1,
//     popup_corners: 5,
//     popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
//     popup_nocss: "no",
    
//     //Advanced settings
//     div: "mapa",
//     auto_load: "yes",
//     url_new_tab: "no",
//     images_directory: "default",
//     fade_time: 0.1,
//     link_text: "View Website",
//     popups: "detect",
//     state_image_url: "",
//     state_image_position: "",
//     location_image_url: "http://nvs.markello.biz/map.png"
//   },
//   state_specific: {
//     KAZ3195: {
//       name: "Актюбинская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3196: {
//       name: "Костанайская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3197: {
//       name: "Кызылординская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3198: {
//       name: "Атырауская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3201: {
//       name: "Западно-Казахстанская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3202: {
//       name: "Акмолинская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3203: {
//       name: "Карагандинская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3204: {
//       name: "Северо-Казахстанская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3205: {
//       name: "Павлодарская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3206: {
//       name: "Восточно-Казахстанская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3207: {
//       name: "Алматинская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3236: {
//       color: "#c6d7e8",
//       name: "Мангистауская область",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3251: {
//       name: "Туркестанская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },
//     KAZ3252: {
//       name: "Жамбылская область",
//       color: "#c6d7e8",
//       description: "Количество волонтеров: 20",
//       url: "http://www.google.ru"
//     },

//   },
//   locations: {
//     "0": {
//       lat: "52.3166599",
//       lng: "76.8947627",
//       name: "Павлодар",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "1": {
//       lat: "43.2173885",
//       lng: "76.8040827",
//       name: "Алма-ата",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "2": {
//       lat: "51.1480774",
//       lng: "71.3393081",
//       name: "Астана",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "3": {
//       lat: "42.341926",
//       lng: "69.519766",
//       name: "Шымкент",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "4": {
//       lat: "49.8241195",
//       lng: "73.0288872",
//       name: "Караганда",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "5": {
//       lat: "50.2737724",
//       lng: "57.1237494",
//       name: "Актобе",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "6": {
//       lat: "42.8962377",
//       lng: "71.2988216",
//       name: "Тараз",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "7": {
//       lat: "52.3166599",
//       lng: "76.8947627",
//       name: "Павлодар",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "8": {
//       lat: "49.9666298",
//       lng: "82.5361189",
//       name: "Усть-Каменогорск",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "9": {
//       lat: "50.4130211",
//       lng: "80.2054884",
//       name: "Семей",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "10": {
//       lat: "53.2055331",
//       lng: "63.551787",
//       name: "Костанай",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "11": {
//       lat: "44.8281369",
//       lng: "65.4700314",
//       name: "Кызылорда",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "12": {
//       lat: "51.2182048",
//       lng: "51.3160907",
//       name: "Уральск",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "13": {
//       lat: "54.8739717",
//       lng: "69.0904091",
//       name: "Петропавловск",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "14": {
//       lat: "43.659588",
//       lng: "51.175688",
//       name: "Актау",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "15": {
//       lat: "49.567335",
//       lng: "72.879909",
//       name: "Темиртау",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "16": {
//       lat: "43.3037389",
//       lng: "68.2255693",
//       name: "Туркестан",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "17": {
//       lat: "53.2989393",
//       lng: "69.3754431",
//       name: "Кокшетау",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "18": {
//       lat: "45.0106102",
//       lng: "78.3548351",
//       name: "Талдыкорган",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "19": {
//       lat: "51.7261692",
//       lng: "75.2844142",
//       name: "Экибастуз",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     },
//     "20": {
//       lat: "52.975533",
//       lng: "63.0861993",
//       name: "Рудный",
//       type: "image",
//       size: "50",
//       opacity: "1"
//     }
//   },
//   labels: {},
//   regions: {}
// };
//jQueryStart
$(function(){
	var winHash = window.location.hash;
	if(winHash !== "" && winHash !== "#"){
		var position = 0;
		if($(winHash).is(winHash)){
			position = $(winHash).offset().top;
		}
		$('html, body').scrollTop(0);
		$('html, body').animate({
        	scrollTop: position,
    	}, 600);
	}
	setBackgroundImage();
	$('.js-range-slider').ionRangeSlider();
	$('.nice-select').niceSelect();
	$('.days').niceSelect();
	$('.mouth').niceSelect();
	$('.year').niceSelect();
	function blockButtonCheckbox(){
		$('.d-lastnews__polit input').each(function(){
			if($(this).is(':checked')){
				$(this).closest('form').find('.red-button').removeClass('disabled');
			}else{
				$(this).closest('form').find('.red-button').addClass('disabled');
			}
		})
	}
	blockButtonCheckbox();
	$('body').on('change','.d-lastnews__polit',blockButtonCheckbox);
	getArrayDays = function(mouth, year){
    	var result = [];
    	var days = new Date(+year,+mouth,0).getDate();
    	var currentYear = new Date().getFullYear();
    	var currentMouth = new Date().getMonth() + 1;
    	var currentDay = new Date().getDate();
    	if(currentYear === +year && currentMouth === +mouth){
	    	return currentDay;
    	}else{
	    	return days;
    	}
    };
    $('body').append('<div class="mobile-menu"></div>');
    $('.mobile-menu').append('<div class="mobile-logo"></div>');
    $('.mobile-logo').append($('header .logo').parent().clone());
    $('.mobile-logo').append('<div class="popup__close-x close-popup">&times;</div>');
    $('.mobile-menu').append('<div class="mobile-header"></div>');
    $('.mobile-menu').append('<div class="mobile-buttons"></div>');
    $('.mobile-header').append($('header .header__profile-container').clone());
    $('.mobile-header').append($('header .red-button').clone());
    $('.mobile-header').append($('header .header__noutificate').clone());
    $('.mobile-header').find('.header__noutificate').addClass('mobile');
    $('.mobile-buttons').append($('header .social-links').clone());
    $('.mobile-buttons').append($('header .lang').clone());
    $('.mobile-menu').append($('nav .search').clone());
    $('.mobile-menu').append('<div class="mobile-links"></div>');
    $('.mobile-links').append($('nav .nav__link').clone());
    $('body').on('click', '.mobile-menu .nav__link a', function(e){
    	if($(this).siblings('.nav__container').is('.nav__container')){
    		e.preventDefault();
    		$(this).siblings('.nav__container').slideToggle();
    	}
    });

    // $('body').on('click','.script-confetti',function(e){
    // 	e.preventDefault();
    // 	$(this).addClass('point-none');
    // 	confettyRun();
    // 	var url = $(this).attr('href');
    // 	setTimeout(function(){
    // 		location.href = url;
    // 	}, 1500);
    // });
    // $('body').on('click','.script-confetti canvas', function(e){
    // 	e.stopPropagation();
    // 	e.preventDefault();
    // })
	$('body').on('click','.lang__current',function(){
		$(this).siblings('.lang__container').slideToggle().css('display','flex');
	});
	var totalSlide = $('.d-slider .swiper-slide').length;
	$('.d-slider .swiper-slide').each(function(index){
		$(this).find('.d-slider-pagination-current').text(index + 1);
		$(this).find('.d-slider-pagination-total').text(totalSlide);
	});
	$('.d-slider .swiper-container').each(function(){
		var swiper = new Swiper($(this), {
			speed: 1200,
	      	autoplay: {
			    delay: 3000,
			},
			loop: true,
			navigation: {
		        nextEl: '.d-slider-right',
		        prevEl: '.d-slider-left',
		    },
	    });
	    $(this).mouseenter(function(){
		    swiper.autoplay.stop();
		});
		$(this).mouseleave(function(){
		    swiper.autoplay.start();
		});

	});
	$('[data-percent-bar]').each(function(){
		var percentBar = +$(this).attr('data-percent-bar');
		if(isNaN(percentBar)){
			console.error('Ошибка значения процента статусбара -', $(this));
		}else{
			$(this).find('div').css('width', percentBar + '%').find('span').text(percentBar + '%');
			if(percentBar < 50){
				$(this).find('div').addClass('right');
			}
		}

	});
	$('.d-direction .swiper-container').each(function(){
		var prev = $(this).parent().find('.prev');
		var next = $(this).parent().find('.next');
		var swiper = new Swiper($(this), {
	      	slidesPerView: 1,
	      	spaceBetween: 32,
	      	speed: 1200,
	      	autoplay: {
			    delay: 2500,
			},
			navigation: {
		        nextEl: next,
		        prevEl: prev,
		    },
		    breakpoints:{
		    	800:{
		    		slidesPerView: 2,
		    	},
		    	1200:{
		    		slidesPerView: 3,
		    	},
		    	1400:{
		    		slidesPerView: 4,
		    		
		    	}
		    }
	    });
	    $(this).mouseenter(function(){
		    swiper.autoplay.stop();
		});
		$(this).mouseleave(function(){
		    swiper.autoplay.start();
		});
	});
	$('.choise .swiper-container').each(function(){
		var prev = $(this).parent().find('.prev');
		var next = $(this).parent().find('.next');
		var swiper = new Swiper($(this), {
	      	slidesPerView: 1,
	      	spaceBetween: 30,
	      	navigation: {
		        nextEl: next,
		        prevEl: prev,
		    },
		    breakpoints:{
		    	1200:{
		    		spaceBetween: 80,
	      			slidesPerView: 3,
		    	},
		    	400:{
	      			slidesPerView: 2,
		    	}
		    }
	    });
	});
	$('.d-target__slider .swiper-container').each(function(){
		var prev = $(this).parent().find('.prev');
		var next = $(this).parent().find('.next');
		var swiper = new Swiper($(this), {
	      	slidesPerView: 7,
  			spaceBetween: 30,
  			speed: 800,
	    	autoplay: {
			    delay: 2000,
			},
			navigation: {
		        nextEl: next,
		        prevEl: prev,
		    },
			loop: true,
			breakpoints:{
		    	1400:{
	      			slidesPerView: 7,
		  			spaceBetween: 46,
		    	},
		    	1200:{
	      			slidesPerView: 6,
		    	},
		    	800: {
	      			slidesPerView: 4,
		    	},
		    	550:{
	      			slidesPerView: 3,
		    	},
		    	320: {
	      			slidesPerView: 2,
		    	}
		    }
	    });
	    $(this).mouseenter(function(){
		    swiper.autoplay.stop();
		});
		$(this).mouseleave(function(){
		    swiper.autoplay.start();
		});
	});
	$('.d-partners .swiper-container').each(function(){
		var prev = $(this).parent().find('.prev');
		var next = $(this).parent().find('.next');
		var swiper = new Swiper($(this), {
	      	slidesPerView: 1,
  			spaceBetween: 20,
	      	navigation: {
		        nextEl: next,
		        prevEl: prev,
		    },
		    breakpoints:{
		    	1200:{
	      			slidesPerView: 5,
		    	},
		    	800: {
	      			slidesPerView: 4,
		    	},
		    	550:{
	      			slidesPerView: 3,
		    	},
		    	430:{
	      			slidesPerView: 2,
		    	},
		    	320: {
	      			slidesPerView: 1,
		    	}
		    }
	    });
	});
	$('body').on('click', '.reg__tab', function(){
		$(this).addClass('active').siblings('.reg__tab').removeClass('active');
		$(this).closest('.reg__body').find('.reg__container > form').removeClass('active').eq($(this).index()).addClass('active');
	});
	$('body').on('click', '.password-input .password-eye', function(){
		if($(this).is('.active')){
			$(this).removeClass('active');
			$(this).parent().find('input').attr('type','password');
		}else{
			$(this).addClass('active');
			$(this).parent().find('input').attr('type','text');
		}
	});
	$('.status-bar').each(function(){
		var elem = $(this);
		$(this).parent().find('input').on('focus', function(){
			elem.addClass('visible');
		});
		$(this).parent().find('input').on('blur', function(){
			if($(this).val().length == 0){
				elem.removeClass('visible');
			}
		});
		$(this).parent().find('input').on('input',function(){
			if($(this).val().length > 5){
				elem.addClass('success');
				elem.removeClass('error');
			}else if($(this).val().length <= 5 && $(this).val().length > 0){
				elem.addClass('error');
				elem.removeClass('success');
			}else{
				elem.removeClass('error');
				elem.removeClass('success');
			}
			if($(this).val().length < 11){
				var per = +$(this).val().length * 10;
				elem.find('div div').css('width', per + "%");
			}else if($(this).val().length > 10){
				elem.find('div div').css('width',"100%");
			}
		});
	});
	$('.d-file input').on('input',function(){
		$(this).siblings('span').text($(this)[0].files[0].name)
	});
	function readURL(input, jqueryThis) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            jqueryThis.closest('.lk-photo').find('img').attr('src', e.target.result);
				setBackgroundImage();
	        };
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$(".imgInput").change(function(){
	    readURL(this, $(this));
	});
	$('.selects__date').on('change','.year',function(){
   		var selectedYear = +$(this).val();
   		var selectedMouth = +$(this).siblings('.mouth').val();
   		var selectedDay = +$(this).siblings('.days').val();
   		var currentYear = new Date().getFullYear();
    	var currentMouth = new Date().getMonth() + 1;
    	if(selectedYear === currentYear){
    		$(this).siblings('select.mouth').find('option').each(function(index){
    			if(currentMouth < (index + 1)){
    				$(this).attr('disabled','true');
    			}
    		});
    		if(selectedMouth > currentMouth){
    			$(this).siblings('select.mouth').find('option').removeAttr('selected');
    			$(this).siblings('select.mouth').find('option').eq(0).attr('selected', 'true');
    			selectedMouth = 1;
    		}
    	}else{
			$(this).siblings('.mouth').find('option').removeAttr('disabled');
    	}
    	var currentDays = getArrayDays(selectedMouth, selectedYear);
    	$(this).siblings('select.days').find('option').each(function(index){
			if(currentDays < (index + 1)){
				$(this).attr('disabled','true');
			}else{
				$(this).removeAttr('disabled');
			}
		});
		if(selectedDay > currentDays){
			$(this).siblings('select.days').find('option').removeAttr('selected');
			$(this).siblings('select.days').find('option').eq(0).attr('selected', 'true');
		}
    	$('select').niceSelect('update');
   });
   $('.selects__date').on('change','.mouth',function(){
   		var selectedMouth = +$(this).val();
   		var selectedYear = +$(this).siblings('.year').val();
   		var selectedDay = +$(this).siblings('.days').val();
    	var currentDays = getArrayDays(selectedMouth, selectedYear);
    	$(this).siblings('select.days').find('option').each(function(index){
			if(currentDays < (index + 1)){
				$(this).attr('disabled','true');
			}else{
				$(this).removeAttr('disabled');
			}
		});
		if(selectedDay > currentDays){
			$(this).siblings('select.days').find('option').removeAttr('selected');
			$(this).siblings('select.days').find('option').eq(0).attr('selected', 'true');
		}
    	$('select').niceSelect('update');
   });
    if($('.selects__date').is('.selects__date')){
    	$('.selects__date').each(function(){
    		var years = +$(this).find('.year').data('min-year');
    		var yearsHtml = '';
    		var mouthHtml = '';
    		var daysHtml = '';
    		var currentYear = new Date().getFullYear();
    		var currentMouth = new Date().getMonth() + 1;
    		for (var i = currentYear; i >= years; i--){
    			yearsHtml += '<option value="'+i+'">'+i+'</option>';
    		}
    		$(this).find('.year').append(yearsHtml);
    		mouthForSelect.forEach(function(item, index){
    			var val = index+1;
    			if(val > currentMouth){
					mouthHtml += '<option value="'+ val +'" disabled>'+item+'</option>';
    			}else{
					mouthHtml += '<option value="'+ val +'">'+item+'</option>';
    			}
    		});
    		$(this).find('.mouth').append(mouthHtml);
    		for(var i = 1; i <= 31; i++){
    			daysHtml += '<option value="'+i+'">'+i+'</option>';
    		}
    		$(this).find('.days').append(daysHtml);
    		var currentDays = getArrayDays(currentMouth, currentYear);
	    	$(this).find('select.days').find('option').each(function(index){
				if(currentDays < (index + 1)){
					$(this).attr('disabled','true');
				}else{
					$(this).removeAttr('disabled');
				}
			});
    		$('select').niceSelect('update');
    	});
    }
    if($('[data-curent-value]').is('[data-curent-value]')){
    	$('[data-curent-value]').each(function(){
	        var val = $(this).attr('data-curent-value');
	        if(val.length > 0){
	            $(this).val(val);
	        }
	    });
	    $('.year').each(function(){
	    	var selectedYear = +$(this).val();
	   		var selectedMouth = +$(this).siblings('.mouth').val();
	   		var selectedDay = +$(this).siblings('.days').val();
	   		var currentYear = new Date().getFullYear();
	    	var currentMouth = new Date().getMonth() + 1;
	    	if(selectedYear === currentYear){
	    		$(this).siblings('select.mouth').find('option').each(function(index){
	    			if(currentMouth < (index + 1)){
	    				$(this).attr('disabled','true');
	    			}
	    		});
	    		if(selectedMouth > currentMouth){
	    			$(this).siblings('select.mouth').find('option').removeAttr('selected');
	    			$(this).siblings('select.mouth').find('option').eq(0).attr('selected', 'true');
	    			selectedMouth = 1;
	    		}
	    	}else{
				$(this).siblings('.mouth').find('option').removeAttr('disabled');
	    	}
	    	var currentDays = getArrayDays(selectedMouth, selectedYear);
	    	$(this).siblings('select.days').find('option').each(function(index){
				if(currentDays < (index + 1)){
					$(this).attr('disabled','true');
				}else{
					$(this).removeAttr('disabled');
				}
			});
			if(selectedDay > currentDays){
				$(this).siblings('select.days').find('option').removeAttr('selected');
				$(this).siblings('select.days').find('option').eq(0).attr('selected', 'true');
			}
	    	$('select').niceSelect('update');
	    })
	    $('select').niceSelect('update');
    }
    $('.lk-tabs .lk-tab').on('click',function(){
    	var index = $(this).index();
    	$(this).addClass('active').siblings('.lk-tab').removeClass('active');
    	$(this).parent().siblings('.lk__tabs-containers').find('.lk__tabs-container').removeClass('active').eq(index).addClass('active');
    });
    $('[data-max-val]').each(function(){
    	var maxLimit = +$(this).attr('data-max-val');
    	if(isNaN(maxLimit)){
    		console.error('неверное значение атрибута data-max-val', $(this));
    	}else{
    		$(this).find('input').on('input',function(){
    			var val = $(this).val();
    			if(+val > maxLimit){
					$(this).val(maxLimit);
    			}else if(+val < 0 || val.length > 2 || val == ''){
    				$(this).val('00');
    			}
    		});
    	}
    });
    $('.number-input .add').on('click',function(){
    	var limit = +$(this).parent().attr('data-max-val');
    	var val = +$(this).siblings('input').val();
    	if(isNaN(limit)){
    		val += 1;
    		$(this).siblings('input').val(val);
    	}else{
	    	if(val < limit){
	    		val += 1;
	    		$(this).siblings('input').val(val);
	    	}
    	}
    });
    $('.number-input .reduse').on('click',function(){
    	var val = +$(this).siblings('input').val();
    	if(0 < val){
    		val -= 1;
    		$(this).siblings('input').val(val);
    	}
    });
    $('body').on('click','.open-accept-block', function(e){
    	e.preventDefault();
    	$(this).closest('.a-item').addClass('accept-open');
    })
    $('.a-item').on('click','.d-overlay', function(){
		$(this).closest('.a-item').removeClass('accept-open');
    });
    $('body').on('click','.header__noutificate-icon',function(){
    	$(this).siblings('.header__alert-container').toggleClass('active');
    	if($(this).parent().is('.mobile')){
    		$('.mobile-header').nextAll().toggleClass('hidden');
    	}
    });
    $('.f-checkboxs').find('.f-checkbox').each(function(index){
		if(index > 5){
			$(this).addClass('hide');
			$(this).closest('.f-checkboxs').addClass('more');
		}
    });
  	$('body').on('click','.f-checkbox-loadmore',function(){
		  $(this).closest('.f-checkboxs').removeClass('more').find('.f-checkbox').removeClass('hide');
    });
    if($('.cookie-script').is('.cookie-script')){
	    if(!(localStorage.getItem('cookieNoutif') === 'yes')){
	    	$('.cookie-block').removeClass('hidden');
	    }
	    $('body').on('click','.cookie-btn',function(){
	    	localStorage.setItem('cookieNoutif', 'yes');
	    	$('.cookie-block').addClass('hidden');
	    });
    }
    function readURLonly(input, jQueryThis) {
	    if (input.files && input.files[0]) {
	        var arrayFilesReader = new FileReader();
            arrayFilesReader.onload = function (e) {
             	jQueryThis.siblings('.img-input__img').find('img').attr('src', e.target.result);
            	setBackgroundImage();
            };
            arrayFilesReader.readAsDataURL(input.files[0]);
            var fileName = input.files[0].name;
            if(fileName.length > 14){
        		fileName = '...'+fileName.slice(-11,fileName.length);
            }
            jQueryThis.siblings('.img-input__filename').text(fileName)
	    }
	}
	$('body').on('click','.img-input__reelect', function(){
		$(this).siblings('input').trigger('click');
	});
	$('body').on('click','.img-input__delete',function(){
		$(this).siblings('input').val("");
		$(this).parent().addClass('hide');
		$(this).closest('.img-inputs').siblings('.d-file-download').removeClass('hide');
	});
	$('body').on('click','.d-file-download .btn-file',function(){
		$(this).parent().siblings('.img-inputs').find('.hide').first().find('input').trigger('click');
	});
	$('.img-input input').on('change',function(e){
		if(typeof e.target.files[0] == 'undefined'){
	      	$(this).parent().addClass('hide');
			$(this).closest('.img-inputs').siblings('.d-file-download').removeClass('hide');
	    }else{
		    readURLonly(this, $(this));
		    $(this).parent().removeClass('hide');
		    if($(this).closest('.img-inputs').find('.img-input.hide').length == 0){
				$(this).closest('.img-inputs').siblings('.d-file-download').addClass('hide');
		    }
	    }
	});
})
