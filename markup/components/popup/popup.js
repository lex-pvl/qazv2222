
$(function(){
	var popup = $('.popup');
	var overlay = $('.overlay');
	var width = $(window).width();
	var close = $('.close-popup');
	var popupOpen;
	var popupClose;

	popupOpen = function(popupName){
		$('body').css('width', width);
		$('body').addClass('no-scroll');
		popup.removeClass('popup__active');
		$('.'+popupName).addClass('popup__active');
		overlay.show();
	}
	popupClose = function(){
		popup.removeClass('popup__active');
		$('.mobile-menu').removeClass('active');
		overlay.hide();
		$('body').removeClass('no-scroll');
	}
	close.on('mousedown', function(event){
		event.preventDefault();
		popupClose();
	});
	overlay.on('mousedown', function(){
		popupClose();
	});
	$('body').on('click','[data-popup-open]',function(e){
		e.preventDefault();
		var popupName = $(this).data('popup-open');
		popupOpen(popupName);
	});
	$('body').on('click','.mobile-menu-btn',function(){
		overlay.show();
		$('.mobile-menu').addClass('active');
	});
});

$(function(){
	// $('.can-input').on('change', function() {
	// 	if (!$('.can-input:checked').length) {
	// 		console.log(false)
	// 		$('.apply').addClass('disabled');
	// 		$('.apply').attr('disabled', true);
	// 		return false;
	// 	} else {
	// 		console.log(true)
	// 		$('.apply').removeClass('disabled');
	// 		$('.apply').attr('disabled', false);
	// 		return true;
	// 	}
	// });
});

$(function(){
	$('#deleted-form').on('submit', function(e) {
		var text = $('#deleted-input').data('text');
		var value = $('#deleted-input').val();
		console.log(value);
		if ( value !== text) {
			e.preventDefault();
			$('.error-text').text('Неверно введено слово УДАЛИТЬ, пожалуйста, повторите попытку');
			$('#deleted-input').addClass('error');
		}
		else {
			$('.error-text').text('');
			$('#deleted-input').removeClass('error');
		}
	});
});

$(function(){
	$.mask.definitions['H'] = "[0-2]";
	$.mask.definitions['h'] = "[0-9]";
	$.mask.definitions['M'] = "[0-5]";
	$.mask.definitions['m'] = "[0-9]";

	$("#l-time").mask("Hh:Mm", {
		completed: function() {
			var currentMask = $(this).mask();
			if (isNaN(parseInt(currentMask))) {
				$(this).val("");
			} else if (parseInt(currentMask) > 2359) {
				$(this).val("23:59");
			};
		}
	});
});


$('.history-counter').each(function(){
	var thLength = $(this).siblings('textarea').attr('maxlength');
	$(this).find('.max').text(thLength);

	$(this).siblings('textarea').on('input', function(){
		var textLen = $(this).val().length;
		$(this).siblings('.history-counter').find('.curent').text(textLen)
	});
});

function formatState(state) {
	if (!state.id) {
		return state.text;
	}
	var $state = $(
		`<span class="zxc">
		<label class="option-label" for="fd">${state.text}</label>
		<input type="checkbox" id="fd" class="option-check" value="${state.element.value}">
		</span>
		`
		);
	return $state;
}



$('#historyFile').fileupload({
	dataType: 'json',
	done: function (e, data) {
		$.each(data.result.files, function (index, file) {
			$('<p/>').text(file.name).appendTo(document.body);
		});
	}
});